import React from 'react';
import './App.css';



class App2 extends React.Component {
  
  constructor(props){
    
    super(props);
    this.state = {
      todo:props.todo
    };
    
    this.elementchange= this.elementchange.bind(this);
  }
  
  
  elementchange() {
    
    // 追加
    this.state.todo.push({
      title: '変更はろー'
    });
    //ES6だけどreactのために書いている(stateをセットしている)
    this.setState({
      todo : this.state.todo
    }); 
  }
  render() {
    console.log("App2render呼び出し");  
    console.log(this.state.todo);
    return (
      <div id="d1"className="font_test2">
            <input  className="font_test2" type="button"onClick={this.elementchange}  value="変わる" />          
      </div>
    );
  }
}

export default App2;