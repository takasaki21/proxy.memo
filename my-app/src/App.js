// import React, { Component } from 'react';
import React from 'react';
// import logo from './logo.svg';
import './App.css';
import App2 from './App2';

//classの宣言
class App extends  React.Component {
  //ES6の書き方(コンストラクタの宣言stateを使うときに必要？)
  constructor(props){
    super(props);
    //ES6だけどreactのために書いている(stateを直接代入してる、コンストラクタだけに許される特権)
    this.state = {
      todo: [
      ]
    };
        //ES6だけどreactのために書いている(renderのinputタグにバインドしてる、しないとundefinedするエラーする)
    this.addElement = this.addElement.bind(this);
  }
  //ライフサイクルメソッド？
  addElement() {  
    // 追加
    this.state.todo.push({
      title: 'はろー'
    });
    //ES6だけどreactのために書いている(stateをセットしている)
    this.setState({
      todo : this.state.todo
    }); 
  }
  render() {
    console.log("Apprender呼び出し");
    console.log(this.state.todo);
    return (
      <div>
        <input  className="font_test" type="button"onClick={this.addElement}  value="増える" />       
        <ul className="font_test">{this.state.todo.map( (todo, i) => {
          return <p>{todo.title}</p>
        })}</ul>
      <App2 todo={this.state.todo } />
      </div>
      
    );
  }
}
// <ul className="font_test">{this.state.todo.map( (todo, i) => {
  //これはJSXの中でES6で書いてる



//何が起きているか
//1.<App /><App2 />がReactDOM.render()に渡される
//2.reactは<App /><App2 />コンポーネントのコンストラクタを呼び出す
//3.<App /><App2 />の出力がDOMに挿入され、ボタンが押されると、それぞれのライフサイクルメソッドが呼び出され
//はろーを追加してセット  し、更新する

//多分2,3でstateが更新されrenderが呼び出される

//stateを直接代入してはいけない唯一していいのはコンストラクタのみ
//stateを更新したいときは基本setStateを使う
//マウント、バインド
//ライフサイクルが重要(renderはライフサイクルのどこか？)
//仮想DOMがあるやつはライフサイクルがある(仲良し)

export default App;